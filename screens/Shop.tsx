import { useRoute } from "@react-navigation/native";
import React from "react";
import { Text, SafeAreaView, View } from '../components/Themed';
import { MyCarousel } from "../components/MyCarousel";
import { Table, Row, Rows } from 'react-native-table-component';

import {
  StyleSheet,
  TouchableOpacity,
  ScrollView 
} from 'react-native';
import { style } from "@mui/system";


export default function Shop({route, navigation}) {
  let shop = route.params.shop;
  let gallery = shop.img_gallery.split(" ");

  let rowStringsDescription = shop.description.split("\n");
  let tableDataDescription = [];
  rowStringsDescription.forEach(row => {
    tableDataDescription.push(row.split(":"));
  });

  const makeRowsCost = () => {

    let varElements = [];

    tableDataDescription.forEach((item) => {
      varElements.push(<Rows data={[item]} textStyle={styles.text}/>);
    });
 
    return varElements;
  };


  const makeRowsTime = () => {
    let varElements = [];

    let item = shop.time.split(":");
    varElements.push(<Rows data={[item]} textStyle={styles.text}/>);

    return varElements;
  }



  
  return (
    <SafeAreaView style={styles.container}>
      <ScrollView style={styles.container}>
        <View style={styles.container}>
          <View style={{backgroundColor: "#0E0"}}>
            <View style={styles.carouselContainer}>
              <MyCarousel data={gallery}> 
              </MyCarousel>
            </View>

            <View style={styles.titleContainer}>
              <Text style={styles.titleText}>{shop.product}</Text>
            </View>
          </View>

          

          <View style={styles.descriptionContainer}>
            <Text style={styles.descriptionText}>{shop.slogan}</Text>
          </View>
          <View style={styles.containerTable}>
            <Text>Precios</Text>
            <Table borderStyle={{borderWidth: 1.5, borderColor: '#00CC00'}}>
              {makeRowsCost()}
            </Table>
          </View>

          <View style={styles.containerTable}>
            <Text>Horario</Text>
            <Table borderStyle={{borderWidth: 1.5, borderColor: '#00CC00'}}>
              {makeRowsTime()}
            </Table>
          </View>

          <View>
            {/* <TouchableOpacity onPress={() => Linking.openURL(url)} >
              <Text>Click to open Map</Text>
            </TouchableOpacity> */}
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}

{/* <TouchableOpacity style={styles.botton} onPress={() => navigation.navigate('Prueba', {shop: shop}) }>
        </TouchableOpacity> */}

const styles = StyleSheet.create({
  container: {
    height: '100%',
    width: '100%',
  },

  carouselContainer: {
    marginLeft: 'auto',
    marginRight: 'auto',
    marginTop: 10,
    marginBottom: 10,
    backgroundColor: "#0E0"
  },

  image: {
    flex: 1,
    justifyContent: "center",
    resizeMode: "cover"
  },

  titleContainer: {
    marginLeft: "5%",
    marginRight: "5%",
    marginBottom: 10,
    backgroundColor: "#0E0",
  },

  titleText: {
    textAlign: "left",
    fontSize: 30,
    fontWeight: "bold",
  },

  descriptionContainer: {
    marginLeft: "5%",
    marginRight: "5%",
    marginBottom: 10,
    marginTop: 10,
  },

  descriptionText: {
    fontSize: 15,
    textAlign: "justify"
  },

  ubiContainer: {
    marginLeft: "5%",
    marginRight: "5%",
    marginBottom: 15,
    marginTop: 5
  },

  ubiText: {
    color: "red",
    fontWeight: "bold"
  },

  containerTable: { 
    flex: 1, 
    padding: 16,  
    backgroundColor: '#fff' 
  },

  head: { 
    height: 40, 
    backgroundColor: '#f1f8ff' 
  },
  text: { 
    margin: 9 ,
  }

});