
import { StyleSheet } from 'react-native';

import { Text, View } from '../components/Themed';
import { RootTabScreenProps } from '../types';



export default function TabOneScreen({ navigation }: RootTabScreenProps<'TabOne'>) {

  // onPress={testFunction}
  
  return (
    <View style={styles.container}>
    </View>
  );
  
}

const styles = StyleSheet.create({
  container: {
    height: '100%',
    flexDirection: 'column',
  },

  boxSelection: {
    height: '25%',
    width: '100%',
    
  },

  image: {
    flex: 1,
    justifyContent: "center",
  },

  imageTitle: {
    flex: 1,
    justifyContent: "center",
    height: '30%',
    width: '100%',
    textShadowRadius: 10,
  },

  title: {
    marginLeft: '7%',
    textAlign: "left",
    fontWeight: "bold",
    fontSize: 20,
    color: 'white',
  },


});
