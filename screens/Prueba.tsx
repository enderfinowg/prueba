import { useRoute } from "@react-navigation/native";
import React from "react";
import { Text, View } from '../components/Themed';
import { MyCarousel } from "../components/MyCarousel";

import {
  StyleSheet,
  SafeAreaView,
  TouchableOpacity 
} from 'react-native';
import { style } from "@mui/system";


export default function Prueba({route, navigation}) {
  let shop = route.params.shop;

  
  return (
    <SafeAreaView>
      <View style={styles.container}>
        <Text>aaaaaaaaaaaaaaaaaaaa</Text>
      </View>
    </SafeAreaView>
  );
}



const styles = StyleSheet.create({
  container: {
    height: '100%',
    width: '100%',
  },

  carouselContainer: {
    marginLeft: 'auto',
    marginRight: 'auto',
    marginTop: 10,
    marginBottom: 10,
  },

  botton: {
    width: '100%',
    height: '100%',
    marginTop: 10,
    backgroundColor: '#202020',
    flex: 1,
    flexDirection: 'row',
    minHeight: 60
  },

  image: {
    flex: 1,
    justifyContent: "center",
    resizeMode: "cover"
  },

  titleContainer: {
    marginLeft: "5%",
    marginRight: "5%",
  },

  titleText: {
    textAlign: "left"
  },

  descriptionContainer: {
    marginLeft: "5%",
    marginRight: "5%",
  },

  descriptionText: {
    textAlign: "justify"
  },

});
