import { StyleSheet, FlatList, SafeAreaView, TouchableOpacity, ColorPropType} from 'react-native';

import { Text, View } from '../components/Themed';

import axios from "axios";

import { useState } from "react";

import { RootTabScreenProps } from '../types';

import ShopsListItem from '../components/Shops_List_Items';
import { height } from '@mui/system';


async function loadItems() {

  let shops : Array<Object> = [];

  try {
    let response = await axios.get("http://192.168.124.124/comercios");
    if (response.status == 200) {
      shops = response.data;
    }
  }
  catch (e) {
    console.error(e);
  }

  return shops;
}


export default function TabFourScreen({ navigation }: RootTabScreenProps<'TabFour'>) {
  
  const [shops, setShops] = useState([]);
  const [copy, setCopy] = useState([]);
  const [firstTime, setFirstTime] = useState(true);
  const [shopElementsFilter, setShopElementsFilter] = useState([]);
  const [disabled,setDisabled] = useState([false, false, false, false]);
  const [filterStyles,setFilterStyles] = useState(
    [styles.filterButton, styles.filterButton, styles.filterButton, styles.filterButton]
  );

  

  if(firstTime)
  {
    loadItems().then(mon => setShops(mon));
    loadItems().then(mon => setCopy(mon));
    setShopElementsFilter(shops);
    setFirstTime(false);
  }

  const filterForShop = (buttonId, type) => {

    // Disable button
    let newDisabled = [false, false, false, false];
    newDisabled[buttonId] = true;
    setDisabled(newDisabled);
    
    let newStyles = [styles.filterButton, styles.filterButton, styles.filterButton, styles.filterButton];
    newStyles[buttonId] = styles.filterButtonDisabled;
    setFilterStyles(newStyles);

    
    // Filter elements
    setShopElementsFilter([]);

    if(type == "")
    {
      setShops(copy);
      return ;
    }

    copy.forEach(item => {
      if(item.type == type)
      {
        shopElementsFilter.push(item);
      }
    });

    setShops(shopElementsFilter);
  };


  return (
    <View style={styles.mainView}>
      <View style={styles.containerHead}>
        <TouchableOpacity style={filterStyles[0]} disabled={disabled[0]} onPress={() => filterForShop(0, "Souvenir")} >
          <Text> a </Text>
        </TouchableOpacity >
        <TouchableOpacity style={filterStyles[1]} disabled={disabled[1]} onPress={() => filterForShop(1, "Ropa")} >
          <Text> a </Text>
        </TouchableOpacity>
        <TouchableOpacity style={filterStyles[2]} disabled={disabled[2]} onPress={() => filterForShop(2, "Restaurante")} >
          <Text> a </Text>
        </TouchableOpacity>
        <TouchableOpacity style={filterStyles[3]} disabled={disabled[3]} onPress={() => filterForShop(3, "")} >
          <Text> a </Text>
        </TouchableOpacity>
      </View>
      <SafeAreaView
        style={styles.container}
      >   
        <FlatList
          style={styles.list}
          data={shops}
          renderItem={({item}) => <ShopsListItem data={{shop: item, navigation: navigation}} />}
          keyExtractor={item => item.id}
        />
      </SafeAreaView>
    </View>

  );
}


const styles = StyleSheet.create({
  list: {
    width: '90%',
    marginLeft: 'auto',
    marginRight: 'auto',
    marginBottom: 5
  },
  container: {
    width: '100%',
  },

  mainView: {
    height: "100%"
  },

  containerHead: {
    flexDirection: 'row',
    width: '100%',
    backgroundColor: "#00CC00",
    height: '9%'
  },

  filterButton: {
    width: '25%',
  },
  filterButtonDisabled: {
    backgroundColor: '#00FF00',
    width: '25%',
  }
});
