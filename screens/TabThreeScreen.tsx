import { StyleSheet, FlatList, SafeAreaView} from 'react-native';

import { Text, View } from '../components/Themed';

import axios from "axios";

import { useState } from "react";

import MonumentsListItem from '../components/Monuments_List_Items';

import { RootTabScreenProps } from '../types';


async function loadItems() {

  let monumentos : Array<Object> = [];

  try {
    let response = await axios.get("http://192.168.124.124/monumentos");
    if (response.status == 200) {
      monumentos = response.data;
    }
  }
  catch (e) {
    console.error(e);
  }

  return monumentos;
}

export default function TabThreeScreen({ navigation }: RootTabScreenProps<'TabThree'>) {
  
  const [monumentos, setMonumentos] = useState([]);
  const [firstTime, setFirstTime] = useState(true);

  if(firstTime)
  {
    loadItems().then(mon => setMonumentos(mon));
    setFirstTime(false);

  }

  return (
    <View style={styles.mainView}>
      <SafeAreaView
        style={styles.container}
      >   
        <FlatList
          style={styles.list}
          data={monumentos}
          renderItem={({item}) => <MonumentsListItem data={{monument: item, navigation: navigation}} />}
          keyExtractor={item => item.id}
        />
      </SafeAreaView>
    </View>

  );
}


const styles = StyleSheet.create({
  list: {
    width: '90%',
    marginLeft: 'auto',
    marginRight: 'auto',
    marginBottom: 5
  },
  container: {
    width: '100%',
  },

  mainView: {
    height: "100%"
  }
});


