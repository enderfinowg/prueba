import React from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  ImageBackground,
} from "react-native";


export default function Menu({ navigation }) {
  return (
    <View style={styles.container}>
      <View style={styles.boxSelection} > 
        <TouchableOpacity style={styles.botton} onPress={() => navigation.navigate('RutePre', {initial: "TabOne"})}>
          <ImageBackground source={{uri: "https://imgur.com/fT85lAU.jpg"}} style={styles.image}>
            <Text style={styles.title}>Nuestras Mejores Rutas</Text>
          </ImageBackground>
        </TouchableOpacity>
        <TouchableOpacity style={styles.botton} onPress={() => navigation.navigate('RuteMake', {initial: "TabTwo"})}>
          <ImageBackground source={{uri: "https://imgur.com/0n0PF3Q.jpg"}} style={styles.image}>
            <Text style={styles.title}>Crea Tu Propia Aventura</Text>
          </ImageBackground>
        </TouchableOpacity>
        <TouchableOpacity style={styles.botton} onPress={() => navigation.navigate('Monum', {initial: "TabThree"})}>
          <ImageBackground source={{uri: "https://imgur.com/NTIG3dh.jpg"}} style={styles.image}>
            <Text style={styles.title}>Monumentos Y Su Historia</Text>
          </ImageBackground>
        </TouchableOpacity>
        <TouchableOpacity style={styles.botton} onPress={() => navigation.navigate('Shop', {initial: "TabFour"})}>
          <ImageBackground source={{uri: "https://imgur.com/NgjeUPi.jpg"}} style={styles.image}>
            <Text style={styles.title}>Descuentos</Text>
          </ImageBackground>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    height: '100%',
    flexDirection: 'column',
  },

  boxSelection: {
    height: '25%',
    width: '100%',
    
  },

  botton: {
    width: '100%',
    height: '100%'
  },

  image: {
    flex: 1,
    justifyContent: "center",
  },

  imageTitle: {
    flex: 1,
    justifyContent: "center",
    height: '30%',
    width: '100%',
    textShadowRadius: 10,
  },

  title: {
    marginLeft: '7%',
    textAlign: "left",
    marginTop: '30%',
    fontWeight: "bold",
    fontSize: 20,
    color: 'white',
  },
});
