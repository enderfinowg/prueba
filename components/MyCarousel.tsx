import { borderColor } from '@mui/system';
import * as React from 'react';
import {
    Text,
    StyleSheet, 
    View,
    Image
} from 'react-native';

import Carousel from 'react-native-snap-carousel';

export class MyCarousel extends React.Component {

    constructor(props) {
        super(props);

        this.sliderWidth = 350;
        this.itemWidth = 350;
        
        this.state = {
            activeIndex: 0,
            carouselItems: props.data
        }
    }





    
    
    _renderItem = ({item, index}) => {
        return (
            <View style={styles.itemContainer}>
              <Image source={{uri: item}} style={styles.image} ></Image>
            </View>
        );
    }

    
    render () {

        return (
            

            <Carousel
              ref={(c) => { this._carousel = c; }}
              data={this.state.carouselItems}
              renderItem={this._renderItem}
              sliderWidth={this.sliderWidth}
              itemWidth={this.itemWidth}
              onSnapToItem={(index) => this.setState({ activeSlide: index })}
              loop={true}
              loopClonesPerSide={2}
              autoplay={true}
              autoplayDelay={1000}
              autoplayInterval={4000}
            />

            
        );
    }
}


const styles = StyleSheet.create({

    itemContainer: {
        width:  '100%',
        height: 250,
    },
  
    image: {
      flex: 1,
      justifyContent: "center",
      resizeMode: "cover"
    },

  });
  