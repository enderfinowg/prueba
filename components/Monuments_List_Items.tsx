import React from "react";
import {
    StyleSheet,
    TouchableOpacity,
    Image
  } from "react-native";

import { Text, View, ViewContainer } from '../components/Themed';

import { NavigationContainer } from '@react-navigation/native';
import Monumento from '../screens/Monumento';

export default function MonumentsListItem({data})
{
    let monument = data.monument;
    let navigation = data.navigation;

    const descipcion = monument.description;
    return(
        <View>
            <TouchableOpacity style={styles.botton} onPress={() => navigation.navigate('Monument', {monument: monument}) }>
                <Image source={{uri: monument.img_portada}} style={styles.image}>
                </Image>
                <ViewContainer style={styles.contenedorDescripcion}>
                    <Text style={styles.title} ellipsizeMode='tail' numberOfLines={2}>{monument.id}</Text>
                    <Text style={styles.description} ellipsizeMode='tail' numberOfLines={2}>{descipcion}</Text>
                </ViewContainer>
            </TouchableOpacity>
        </View>
    );
}

const styles = StyleSheet.create({
  
    botton: {
      width: '100%',
      height: '100%',
      marginTop: 10,
      backgroundColor: '#202020',
      flex: 1,
      flexDirection: 'row',
      minHeight: 60
    },

    contenedorDescripcion: {
      width: '67%',
      paddingLeft: 15,
      paddingRight: 10
    },
  
    image: {
      flex: 1,
      justifyContent: "center",
      width: 100,
      height: 100,
      minWidth: "35%",
      maxWidth: '35%',
      resizeMode: "cover"
    },
  
    title: {
      textAlign: "left",
      marginTop: '3%',
      marginBottom: 0,
      fontWeight: "bold",
      fontSize: 22,
    },
    description: {
        textAlign: "left",
        fontWeight: "bold",
        fontSize: 12,
        marginTop: 5,
        marginRight: 40,
        // width: 
      },
  });
  
